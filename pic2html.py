# By Derpolino
# 18/08/2016

import urllib2
import re
import requests
from PIL import Image

class pic2html():
    def __init__(self, url, width=None, height=None, characters="#"):
        self.url = url
        self.width = width
        self.height = height
        self.characters = characters
        self.download()

        if self.width and self.height:
            self.resize()

        self.save()
        self.transform()

    def __repr__(self):
        return str(self.picHtml)

    def download(self):
        self.picObject = Image.open(urllib2.urlopen(self.url))

    def resize(self):
        self.picObject = self.picObject.resize((self.width, self.height), Image.ANTIALIAS)

    def save(self):
        from StringIO import StringIO
        output = StringIO()
        self.picObject.save(output, format="PNG")
        self.picData = output.getvalue()
        output.close()

    def transform(self):
        data = {"characters":self.characters,
                "textType":"random",
                "fontsize":"1",
                "width":"150",
                "grayscale":"0",
                "bgcolor":"BLACK",
                "contrast":"0",
                "browser":"firefox"}
        request = requests.post("http://www.text-image.com/convert/pic2html.cgi", files = {"image": self.picData}, data = data)
        p = re.compile(ur'<pre>((?:.|\n)*?)</pre>')
        self.picHtml = re.search(p, request.text).group(0).split("\n")[0][5:]
        self.picHtml = re.sub('<font color=(.*?)>', "<font color='\g<1>'>", self.picHtml)
