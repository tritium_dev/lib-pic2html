# Pic2html
## Introduction

Pic2html va vous permettre de transformer une image à partir d'une url, en html **coloré** !

## Requirements

Voici les libs requisent

 - Python 2.7-3
 - Python Image Librairie (PIL) ou Pillow
 - urllib2
 - regex
 - requests

## Fonctionnement

Cette lib utilise le site [www.text-image.com](http://www.text-image.com) pour transformer l'image en html, vous devez donc avoir une connexion internet.

## Utilisation

Voici un code d'exemple :

```py
from pic2html import pic2html
image = pic2html("http://www.text-image.com/samples/eye-small.jpg")
print(image)
```

Arguments possibles :
```py
pic2html(url, width=None, height=None, characters="#")
```
